#include "calculator.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app ( argc , argv );
    Calculator c;
    c . setWindowTitle ("Calculator") ;
    c . resize (300 , 300) ;

    c.show();
    return app.exec();
}
