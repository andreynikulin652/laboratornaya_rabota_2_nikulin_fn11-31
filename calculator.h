#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QtWidgets>
#include <QObject>
#include <QWidget>
#include <QtMath>
#include <QLCDNumber>
#include <QMainWindow>
#include <QCoreApplication>
#include <QGuiApplication>
#include <QPushButton>
#include <QRadioButton>
#include <QSpacerItem>
#include <QMessageBox>
#include <QSize>
#include <QHBoxLayout>
#include <QWindow>
#include <QMenu>
#include <QApplication>

using namespace Qt;

class my_widget;


class Calculator : public QMainWindow
{
    Q_OBJECT
public:
Calculator(QWidget *parent = nullptr);
   ~Calculator();

};




class my_widget:public QWidget
{
    Q_OBJECT
    QPushButton *one,*two,*three,*four, *five, *six, *seven, *eight, *nine, *zero;
    QPushButton *ac;

     QLCDNumber* number;
public: my_widget();
public slots:
     void change_value(int);
    void click_operation(const QString&);
    void click_CE(const QString&);
private slots:
      void slotButton1();
signals:
     void signalFromButton(int);
    void s_click_operation(const QString&);
    void s_click_CE(const QString&);
};





#endif // CALCULATOR_H

